#!/bin/bash

username=$USER
userdata=$(curl -X GET http://localhost:8888/users/"${username}")

ID=$(echo ${userdata} | jq '.id')
USERNAME=$(echo ${userdata} | jq -r '.username')
DOCKER_IMAGE=$(echo ${userdata} | jq -r '.docker_image')
VERSION=$(echo ${userdata} | jq -r '.version')
GPU_DEVICE=$(echo ${userdata} | jq -r '.gpu_device')
PORT_RANGE=$(echo ${userdata} | jq -r '.port_range')

if [ "$USERNAME" == "null" ]; then
  echo "Invalid User"
else
  echo "Start to Run Container... [PORT_RANGE: $PORT_RANGE]"
  if [ ! "$(docker ps -q -f name=${USER})" ]; then
    if [ "$(docker ps -aq -f status=exited -f name=${USER})" ]; then
      # start container
      docker start $USER 1>>/var/log/user/custom.log
    else
      # run container
      docker run -it -d \
        -l comcom \
        -l VERSION=${VERSION} \
        -e HOME=/home/${USER} \
        -v ${HOME}:${HOME} \
        -v /shared:${HOME}/shared:ro \
        -v /raid/work:${HOME}/work \
        --gpus device=${GPU_DEVICE} \
        --name ${USER} \
        -p ${PORT_RANGE}:${PORT_RANGE} \
        --restart unless-stopped \
        --network my-bridge \
        ${DOCKER_IMAGE} /bin/bash 1>>/var/log/user/custom.log
    fi
  fi
  docker exec -i -t --workdir /home/${USER} ${USER} /bin/bash
fi
